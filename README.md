# RaspberryPi Rack

> scad files for a complete four compute units + power unit + network switch raspberry pi rack

Heavily based on the awesome work of [KronBjorn](https://github.com/KronBjorn) and his [six inch rack project](https://github.com/KronBjorn/SixInchRack).

## License

MIT © [Häfner Technology](https://haefner.technology)

