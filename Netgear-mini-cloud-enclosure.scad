  ///////////////////////
 // Netgear GS105 //
///////////////////////

// Increase precision for circles etc.
$fn = 50;

print_handle     = false;
print_frontplate  = false;
print_cabinet     = true;
print_lid          = false;
print_rear         = true;

sixinch      = 155*1;       // cm = 6"
unit         = (44.5/19)*6; // 1U
units        = 2;
screw_hole  = 2.6;

if(print_handle){
    translate([22,2,0]){rotate([0,180,0]){handle();}} // right handle
    translate([sixinch-14,2,0]){rotate([0,180,0]){handle();}} // left handle
}

if(print_frontplate){
    frontplate(units);
}

depth = 150;
if(print_cabinet){
    union() {
        cabinet(depth);
        // ensure switch cant push over the edge in the floor
        translate([22,2,42]){cube([10,6,4]);}
        translate([88,2,42]){cube([30,6,4]);}
    }
}

if(print_lid){
    lid(depth);
}

if(print_rear){
    square_hole  = [
        [ 8, 4, 82, 17], // LAN-Ports 1-5
        [93, 4, 12, 15], // Power
    ];
    back_plate(square_hole);
}

module handle(){
    difference(){
        // stample out the inner part with a smaller and translated version of the same form
        handle_base_form_top_rounded_cube(8,units*unit-4,10);
        translate([-1,5,-1]){handle_base_form_top_rounded_cube(10,units*unit-10-4,6);}
    }
    // top bold
    translate([4,2.5,-2.5]){          cylinder(r1=(screw_hole/2)-0.5,r2=(screw_hole/2)-0.2,h=2.5);}
    // bottom bold
    translate([4,units*unit-4-2.5,-2.5]){ cylinder(r1=(screw_hole/2)-0.5,r2=(screw_hole/2)-0.2,h=2.5);}
}

module handle_base_form_top_rounded_cube(width,height,d){
    radius = 3;
    translate([0,radius,d-radius]){
        rotate([0,90,0]){
            cylinder(r=radius,h=width);
        }
    }
    translate([0,height-radius,d-radius]){
        rotate([0,90,0]){
            cylinder(r=radius,h=width);
        }
    }
    translate([0,radius,0]){
        cube([width,height-radius-radius,d]);
    }
    cube([width,height,d-radius]); // basic cube
}

screw_hole = 2.6;
gauge      = 3*1;
gauge_box = 2*1;
width      = 155-20-20;   // 11.5cm between rails
module frontplate(u){

    difference(){
        baseplate(u);

        //Cabinet holes
        frontholes(u);

        //Handle holes
        translate([18,4.5,0]){cylinder(r=screw_hole/2,h=gauge);}
        translate([18,u*unit-4.5,0]){cylinder(r=screw_hole/2,h=gauge);}
        translate([sixinch-18,4.5,0]){cylinder(r=screw_hole/2,h=gauge);}
        translate([sixinch-18,u*unit-4.5,0]){cylinder(r=screw_hole/2,h=gauge);}
    }

    //cabinet support bars
    translate([0,0,gauge]){
        translate([20+width*0.25,gauge_box,0]){  cube([width*0.5,1,1.5]);  }
        translate([20+width*0.25,unit*u-1-gauge_box,0]){  cube([width*0.5,1,1.5]);  }
    }
}

module baseplate(u){
    difference(){
        union(){
            translate([1.25,1.25,1.25]){
                minkowski(){
                    cube([sixinch-2.5,unit*u-2.5,gauge-2.5]);
                    sphere(r=1.25);
                }
            }
        }
        //Rack mount holes
        translate([10-0.5,unit/2,-gauge/2])                 {cylinder(r=2.3,gauge*2);}
        translate([sixinch-10+0.5,unit/2,-gauge/2])         {cylinder(r=2.3,gauge*2);}
        translate([10-0.5,u*unit-(unit/2),-gauge/2])        {cylinder(r=2.3,gauge*2);}
        translate([sixinch-10+0.5,u*unit-(unit/2),-gauge/2]){cylinder(r=2.3,gauge*2);}
        if(u>=5){
            translate([10-0.5,(u*unit)/2,-gauge/2])         {cylinder(r=2.3,gauge*2);}
            translate([sixinch-10+0.5,(u*unit)/2,-gauge/2]) {cylinder(r=2.3,gauge*2);}
        }
    }
}

module frontholes(u){
    p = 36;

    // bottom screws
    translate([p,gauge_box+4,-0.01]){screw();}
    translate([sixinch-p,gauge_box+4,-0.01]){screw();}

    // top screws
    translate([p,unit*u-gauge_box-4,-0.01]){screw();}
    translate([sixinch-p,unit*u-gauge_box-4,-0.01]){screw();}
}

screw_head         = 7.4;
screw_head_height = 2.2;
screw_dia          = 3;
module screw(){
    cylinder(r1=screw_head/2, r2=screw_dia/2, h=screw_head_height);
    cylinder(r=screw_dia/2, h=40);
    translate([0,0,-0.99]){cylinder(r=screw_head/2, h=1);}
}

module cabinet(depth){
    difference(){
        box(depth, units);
        netgear();
    }
}

module box(dp,u){
    difference(){
        translate([20,0,gauge]){
            difference(){
                cube([115,u*unit,dp]);
                translate([gauge_box,gauge_box,-1]){ cube([115-gauge_box*2,u*unit,dp+2]);}
            }
        }
        chimney();
    }

    //Screw mounts on the bottom plate
    p = 36;
    translate([p+5,gauge_box,gauge]){rotate([0,-90,0]){screwtrap();}}
    translate([sixinch-p+5,gauge_box,gauge]){rotate([0,-90,0]){screwtrap();}}

    //Screw mounts for lid.
    translate([20+gauge_box,unit*u-gauge_box,gauge+10]){rotate([0,0,-90]){screwtrap();}}
    translate([20+gauge_box,unit*u-gauge_box,gauge+34]){rotate([0,0,-90]){screwtrap();}}
    translate([sixinch-20-gauge_box,unit*u-gauge_box,gauge+10+10]){rotate([180,0,-90]){screwtrap();}}
    translate([sixinch-20-gauge_box,unit*u-gauge_box,gauge+dp-70]){rotate([180,0,-90]){screwtrap();}}
    translate([sixinch-20-gauge_box,unit*u-gauge_box,gauge+dp-10-10+10]){rotate([180,0,-90]){screwtrap();}}

    //Lid rest bar
    translate([20+gauge_box,unit*u-3-gauge_box,gauge]){
        linear_extrude(dp-gauge_box){polygon (points=[[0,0],[3,3],[0,3]]);}
    }
    translate([sixinch-20-gauge_box,unit*u-3-gauge_box,gauge]){
        linear_extrude(dp-gauge_box){polygon (points=[[0,0],[-3,3],[0,3]]);}
    }

    // Back/front rest bar
    translate([0,0,dp+gauge-2-gauge_box]){
        difference(){
            translate([20,0,0]){  cube([width,unit*u-gauge_box,2]);  }
            translate([20+gauge_box+8,gauge_box+4,-1]){  cube([width-8*gauge_box-2,unit*u,4]);  }
        }
    }
}

module screwtrap(){
    difference(){
        translate([0,0,5]){
            intersection(){
                scale([1,1,0.7]){
                    sphere(r=8);
                }
                translate([0,-0.001,-10]){  cube([20,20,20]);}
            }
        }
        translate([2,4,5]){
            rotate([90,90,90]){
               translate([0,0,-10]){ cylinder(r=screw_dia/2,h=20,$fn=100);}
            }
        }
    }
}

module chimney() {
    for(i=[0:8:80]){
        echo(i);
        translate([20 + (width-80)/2  +i,-1,20]){
            minkowski(){
                cube([0.01,200,20]);
                sphere(d=2,h=1);
            }
        }
    }
}

slip = 0.35;
module lid(dp){
    difference(){
        union(){
            p = 36;
            translate([p-5,unit*units-gauge_box,gauge]){rotate([0,-90,180]){screwtrap();}}
            translate([sixinch-p-5,unit*units-gauge_box,gauge]){rotate([0,-90,180]){screwtrap();}}

            //lid
            difference() {
                translate([20+gauge_box+slip,units*unit-gauge_box,gauge]){
                    cube([115-gauge_box*2-slip*2,gauge_box,dp]);
                }
                netgear();
            }
            //back rest
            translate([sixinch/2-40,units*unit-gauge_box-2,gauge+dp-2-gauge_box]){
                cube([80,2,2]);
            }
        }

        //Negative
        translate([20+gauge_box+4,units*unit+0.01  ,gauge+10+5]){rotate([90,0,0]){screw();}}
        translate([20+gauge_box+4,units*unit+0.01  ,gauge+34+5]){rotate([90,0,0]){screw();}}
        translate([sixinch-20-gauge_box-4,units*unit+0.01   ,gauge+10+10-5]){rotate([90,0,0]){screw();}}
        translate([sixinch-20-gauge_box-4,units*unit+0.01  ,gauge+dp-70-5]){rotate([90,0,0]){screw();}}
        translate([sixinch-20-gauge_box-4,units*unit+0.01  ,gauge+dp-10-10+10-5]){rotate([90,0,0]){screw();}}
         chimney();
    }
}

module netgear() { // GS105E-200
    translate([22,0.5,47]){
        cube([96,27,102]);
    }
}

module back_plate(square_hole){
     difference(){
            union(){
                translate([20+gauge_box+slip,gauge_box+slip,gauge+depth-gauge_box]){
                    cube([width-2*gauge_box-2*slip, unit*units-2*gauge_box-2*slip, gauge_box]);
                }
            }

        //Square holes
        for(i=[0:len(square_hole)-1]){
            x = square_hole[i][0];
            y = square_hole[i][1];
            w = square_hole[i][2];
            h = square_hole[i][3];
            translate([x+20+gauge_box,y+gauge_box,depth-1]){cube([w,h,gauge+2]);}
        }
    }
}
